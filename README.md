# Workshop Antares
This repository consists of firmwares (Arduino .ino files) and libraries that are used in Antares Workshop. There are two main programs :
1. Send sensor data
2. Control relay

## Board
1. ESP32
2. ESP8266 (Wemos D1)
3. Arduino Pro Mini

## Connectivity
1. LoRa
2. WiFi (HTTP)

## Libraries
1. Antares ESP32 HTTP
2. Antares ESP8266 HTTP
3. LoRaWAN Arduino
