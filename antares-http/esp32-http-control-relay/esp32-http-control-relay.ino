#include <AntaresESP32HTTP.h>

#define ACCESSKEY "your-access-key"
#define WIFISSID "your-wifi-ssid"
#define PASSWORD "your-wifi-password"

#define projectName "your-project-name"
#define deviceName "your-device-name"

#define RELAY 25

AntaresESP32HTTP antares(ACCESSKEY);

void setup() {
  Serial.begin(115200);
  antares.setDebug(true);
  antares.wifiConnection(WIFISSID,PASSWORD);
  pinMode(RELAY, OUTPUT);
}

void loop() {
  antares.get(projectName, deviceName);
  if (antares.getSuccess()) {
		int status = antares.getInt("status");
		if (status == 1) {
			Serial.println("Relay ON");
      digitalWrite(RELAY, HIGH);
		} else {
			Serial.println("Relay OFF");
			digitalWrite(RELAY, LOW);
		}
  }

  delay(10000);
}
