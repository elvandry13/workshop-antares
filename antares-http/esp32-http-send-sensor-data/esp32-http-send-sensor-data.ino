#include <AntaresESP32HTTP.h>
#include "DHT.h"

#define DHTPIN 14
#define DHTTYPE DHT11

#define ACCESSKEY "your-access-key"
#define WIFISSID "your-wifi-ssid"
#define PASSWORD "your-wifi-password"

#define projectName "your-project-name"
#define deviceName "your-project-name"

AntaresESP32HTTP antares(ACCESSKEY);
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(115200);
  antares.setDebug(true);
  antares.wifiConnection(WIFISSID, PASSWORD);
  dht.begin();
}

void loop() {
  // Variables
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  float f = dht.readTemperature(true);
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
		return;
  }

  // Add variable data to storage buffer
  antares.add("temperature", t);
  antares.add("humidity", h);

  // Send from buffer to Antares
  antares.send(projectName, deviceName);

  delay(10000);
}
